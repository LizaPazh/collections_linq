﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace BSA_LINQ
{
    class Program
    {
        static async Task Main(string[] args)
        {
            ProjectService service = new ProjectService();
            await service.LoadData();

            // 1
            Console.WriteLine("Get the number of tasks from the project of a specific user");
            Console.WriteLine("Input user id:");
            int userId;
            userId = Convert.ToInt32(Console.ReadLine());
            var userProjectsTasks = service.CountTasksForProjectByUser(userId);
            foreach (var item in userProjectsTasks)
            {
                Console.WriteLine($"Project name: {item.Key.Name}  Number of tasks: {item.Value}");
            }

            // 2
            Console.WriteLine();
            Console.WriteLine("Get the list of tasks assigned to a specific user (by id), where name task is < 45 characters");
            Console.WriteLine("Input user id:");
            userId = Convert.ToInt32(Console.ReadLine());
            var tasks = service.TasksByUser(userId);
            foreach (var task in tasks)
            {
                Console.WriteLine($"Task id: {task.Id}, task name: {task.Name}");
            }

            //3
            Console.WriteLine();
            Console.WriteLine("Get the list (id, name) from the collection of tasks that were completed in the current (2020) year for a specific user (by id).");
            Console.WriteLine("Input user id:");
            userId = Convert.ToInt32(Console.ReadLine());
            var finishedTasks = service.FinishedTasksByUser(userId);
            foreach (var task in finishedTasks)
            {
                Console.WriteLine($"Task id: {task.Item1}, task name: {task.Item2}");
            }

            //4
            Console.WriteLine();
            Console.WriteLine("Get the list from the collection of teams whose members are older than 10 years, sorted by user registration date in descending order, as well as grouped by team.");
            var teams = service.GetTeamsByUserAge();
            foreach (var team in teams)
            {
                Console.WriteLine($"Team id: {team.Item1}, team name: {team.Item2}, team users count: {team.Item3.Count()}");
            }

            //5
            Console.WriteLine();
            Console.WriteLine("Get a list of users alphabetically first_name (ascending) with sorted tasks by length name (descending).");
            var users = service.UsersByABC();
            foreach (var user in users)
            {
                Console.WriteLine($"User name: {user.FirstName}");
                Console.WriteLine("User tasks:");
                foreach (var t in user.ProjectTasks)
                {
                    Console.WriteLine($"Task name: {t.Name}");
                }
            }

            //6
            Console.WriteLine();
            Console.WriteLine("Get the following structure (pass the user Id to the parameters): User, Last user project(by creation date), Total number of tasks under the last project, The total number of incomplete or canceled tasks for the user, The longest user task by date");
            Console.WriteLine("Input user id:");
            userId = Convert.ToInt32(Console.ReadLine());
            var userWithParams = service.GetUsersCharacteristics(userId);
            Console.WriteLine($"User name: {userWithParams.User.FirstName}, last project id: {userWithParams.LastProject.Id}, last project tasks count: {userWithParams.LastProjectTasks}, unfinishedUserTasks: {userWithParams.NotCompletedOrCanceledTasks}, longest task id: {userWithParams.LongestTask.Id} ");

            //7
            Console.WriteLine();
            Console.WriteLine("Get the following structure: Project, The longest project task, The shortest project task(by name), The total number of users in the project team, where either the project description > 20 characters or the number of tasks < 3");
            var projecstWithParams = service.GetProjectWithCharacteristics();
            foreach(var p in projecstWithParams)
            {
                Console.WriteLine($"Project id: {p.Project.Id}, longest task by description: {p.TaskByDescription?.Description}, shortest project task by name: {p.TaskByName?.Name}, number of users in the project team: {p.UsersCount} ");
            }
        }

    }
}
