﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BSA_LINQ.Models;

namespace BSA_LINQ
{
    public class ProjectClient
    {

        static readonly HttpClient client = new HttpClient() { BaseAddress = new Uri("https://bsa20.azurewebsites.net/api/") };
       
     
        public async Task<IEnumerable<Project>> GetProjects()
        {
            var projects =  await client.GetStringAsync("projects");
            return JsonConvert.DeserializeObject<IEnumerable<Project>>(projects);
        }
        public async Task<Project> GetProject(int id)
        {
            var project = await client.GetStringAsync($"projects/{id}");
            return JsonConvert.DeserializeObject<Project>(project);
        }
        public async Task<IEnumerable<ProjectTask>> GetTasks()
        {
            var tasks = await client.GetStringAsync("tasks");
            return JsonConvert.DeserializeObject<IEnumerable<ProjectTask>>(tasks);
        }
        public async Task<ProjectTask> GetTask(int id)
        {
            var task = await client.GetStringAsync($"tasks/{id}");
            return JsonConvert.DeserializeObject<ProjectTask>(task);
        }
        public async Task<IEnumerable<TaskStateModel>> GetTaskStates()
        {
            var states = await client.GetStringAsync("taskstates");
            return JsonConvert.DeserializeObject<IEnumerable<TaskStateModel>>(states);
        }
        public async Task<IEnumerable<Team>> GetTeams()
        {
            var teams = await client.GetStringAsync("teams");
            return JsonConvert.DeserializeObject<IEnumerable<Team>>(teams);
        }
        public async Task<Team> GetTeam(int id)
        {
            var team = await client.GetStringAsync($"teams/{id}");
            return JsonConvert.DeserializeObject<Team>(team);
        }
        public async Task<IEnumerable<User>> GetUsers()
        {
            var users = await client.GetStringAsync("users");
            return JsonConvert.DeserializeObject<IEnumerable<User>>(users);
        }
        public async Task<User> GetUser(int id)
        {
            var user = await client.GetStringAsync($"users/{id}");
            return JsonConvert.DeserializeObject<User>(user);
        }
    }
}
