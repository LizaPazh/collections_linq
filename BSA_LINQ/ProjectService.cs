﻿using BSA_LINQ.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA_LINQ
{
    public class ProjectService
    {
        readonly ProjectClient client = new ProjectClient();
        private IEnumerable<Project> _projects;
        private IEnumerable<User> _users;
        private IEnumerable<ProjectTask> _tasks;
        private IEnumerable<Team> _teams;

        public async Task LoadData()
        {
            var projects = await client.GetProjects();
            var users = await client.GetUsers();
            var tasks = await client.GetTasks();
            var teams = await client.GetTeams();
            
            tasks = tasks.Join(users, t => t.PerformerId, u => u.Id, (t, p) => { t.Performer = p; return t; });
            _tasks = tasks;
            users = users.GroupJoin(tasks, u => u.Id, t => t.PerformerId, (u, t) => { u.ProjectTasks = t; return u; });
            _users = users;
            teams = teams.GroupJoin(users, t => t.Id, u => u.TeamId, (t, p) => { t.Users = p; return t; });
            _teams = teams;
            projects = projects.GroupJoin(tasks, p => p.Id, t => t.ProjectId, (p, taskList) => { p.ProjectTasks = taskList; return p; });
            projects = projects.Join(users, p => p.AuthorId, u => u.Id, (p, u) => { p.Author = u; return p; });
            projects = projects.Join(teams, p => p.TeamId, t => t.Id, (p, t) => { p.Team = t; return p; });
            _projects = projects;
        }
        public Dictionary<Project, int> CountTasksForProjectByUser(int id)
        {
            return _projects.Where(p => p.Author.Id == id).ToDictionary(y => y, y => y.ProjectTasks.Count());
        }

        public IEnumerable<ProjectTask> TasksByUser(int id)
        {
            return _tasks.Where(t => t.PerformerId == id && t.Name.Length < 45);
        }

        public IEnumerable<Tuple<int, string>> FinishedTasksByUser(int id)
        {
            return _tasks.Where(t => t.PerformerId == id && t.FinishedAt.Year == DateTime.Now.Year)
                         .Select(c => new Tuple<int, string>(c.Id, c.Name));
        }

        public IEnumerable<Tuple<int, string, IEnumerable<User>>> GetTeamsByUserAge()
        {
            return _teams.Where(t => t.Users.Any() && t.Users.All(u => DateTime.Now.Year - u.Birthday.Year >= 10))
                        .Select(x => { x.Users = x.Users.OrderByDescending(x => x.RegisteredAt); return x; })
                        .GroupBy(x => x)
                        .Select(g => new Tuple<int, string, IEnumerable<User>>( g.Key.Id, g.Key.Name, g.Key.Users));

        }
  
        public IEnumerable<User> UsersByABC()
        {
            return _users.OrderBy(u => u.FirstName).Select(x => { x.ProjectTasks = x.ProjectTasks.OrderByDescending(x => x.Name.Length); return x; });
        }
        
        public UserWithParameters GetUsersCharacteristics(int id)
        {
            var lastProject = _projects.Where(p => p.Author.Id == id).OrderByDescending(p=>p.CreatedAt).FirstOrDefault();
            var notCompletedOrCanceledTasks = _tasks.Where(t => t.PerformerId == id && t.State != TaskState.Finished).Count();
            var longestTask = _tasks.Where(t => t.PerformerId == id).OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault();
            return new UserWithParameters
            {
                User = _users.FirstOrDefault(u => u.Id == id),
                LastProject = lastProject,
                LastProjectTasks = lastProject.ProjectTasks.Count(),
                NotCompletedOrCanceledTasks = notCompletedOrCanceledTasks,
                LongestTask = longestTask
            };
            
        }
        public IEnumerable<ProjectWithParameters> GetProjectWithCharacteristics()
        {
            return _projects.Select(p => new ProjectWithParameters
            {
                Project = p,
                TaskByDescription = p.ProjectTasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                TaskByName = p.ProjectTasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                UsersCount = (p.Description.Length > 20 || p.ProjectTasks.Count() < 3) ? p.Team.Users.Count() : 0
            });
        }
    }

}
