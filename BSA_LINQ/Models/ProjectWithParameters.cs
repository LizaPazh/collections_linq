﻿using BSA_LINQ.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BSA_LINQ
{
    public class ProjectWithParameters
    {
        public Project Project { get; set; }
        public ProjectTask TaskByDescription { get; set; }
        public ProjectTask TaskByName { get; set; }
        public int UsersCount { get; set; }
    }
}
